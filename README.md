# Kaggle courses

https://www.kaggle.com/learn

https://gitlab.com/geza.molnar/kaggle-courses/

## Python

Learn the most important language for data science.

https://www.kaggle.com/learn/python

https://gitlab.com/geza.molnar/kaggle-courses/-/tree/main/Python


Explore Courses
## Intro to Machine Learning
Intro to Machine Learning
Learn the core ideas in machine learning, and build your first models.

Intermediate Machine Learning
Intermediate Machine Learning
Handle missing values, non-numeric values, data leakage, and more.
Pandas
Pandas

Solve short hands-on challenges to perfect your data manipulation skills.
Data Visualization
Data Visualization
Make great data visualizations. A great way to see the power of coding!
Feature Engineering
Feature Engineering
Better features make better models. Discover how to get the most out of your data.
Data Cleaning
Data Cleaning
Master efficient workflows for cleaning real-world, messy data.
Intro to SQL
Intro to SQL
Learn SQL for working with databases, using Google BigQuery.
Advanced SQL
Advanced SQL
Take your SQL skills to the next level.
Intro to AI Ethics
Intro to AI Ethics
Explore practical tools to guide the moral design of AI systems.
Intro to Deep Learning
Intro to Deep Learning
Use TensorFlow and Keras to build and train neural networks for structured data.
Computer Vision
Computer Vision
Build convolutional neural networks with TensorFlow and Keras.
Geospatial Analysis
Geospatial Analysis
Create interactive maps, and discover patterns in geospatial data.
Machine Learning Explainability
Machine Learning Explainability
Extract human-understandable insights from any model.
Microchallenges
Microchallenges
Solve ultra-short challenges to build and test your skill.
Natural Language Processing
Natural Language Processing
Distinguish yourself by learning to work with text data.
Intro to Game AI and Reinforcement Learning
Intro to Game AI and Reinforcement Learning
Build your own video game bots, using classic and cutting-edge algorithms.
